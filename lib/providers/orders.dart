import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:shop_app/providers/cart.dart';
import 'package:http/http.dart' as http;

class OrderItem {
  final String id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  OrderItem({
    @required this.id,
    @required this.amount,
    @required this.products,
    @required this.dateTime,
  });
}

class Orders with ChangeNotifier {
  List<OrderItem> _orders = [];
  final String authToken;
  final String userId;

  Orders(this.authToken, this.userId, this._orders);

  List<OrderItem> get orders {
    return [..._orders];
  }

  Future<void> fetchOrders() {
    final url =
        'https://fluter-update-ede72.firebaseio.com/orders/$userId.json?auth=$authToken';
    return http.get(url).then((response) {
      final extractedOrders =
          json.decode(response.body) as Map<String, dynamic>;
      List<OrderItem> orderItems = [];
      if (extractedOrders == null) return;
      extractedOrders.forEach((orderId, orderData) {
        orderItems.add(
          OrderItem(
            id: orderId,
            amount: orderData['amount'],
            products: (orderData['products'] as List<dynamic>)
                .map(
                  (item) => CartItem(
                    id: item['id'],
                    title: item['title'],
                    quantity: item['quantity'],
                    price: item['price'],
                  ),
                )
                .toList(),
            dateTime: DateTime.parse(orderData['dateTime']),
          ),
        );
      });
      _orders = orderItems;
      notifyListeners();
    });
  }

  Future<void> addOrder(List<CartItem> cartProducts, double total) {
    final url =
        'https://fluter-update-ede72.firebaseio.com/orders/$userId.json?auth=$authToken';
    final timestamp = DateTime.now();
    return http
        .post(url,
            body: json.encode({
              'amount': total,
              'dateTime': timestamp.toIso8601String(),
              'products': cartProducts
                  .map((cp) => {
                        'id': cp.id,
                        'title': cp.title,
                        'quantity': cp.quantity,
                        'price': cp.price
                      })
                  .toList(),
            }))
        .then((response) {
      _orders.insert(
        0,
        OrderItem(
          id: json.decode(response.body)['name'],
          amount: total,
          dateTime: timestamp,
          products: cartProducts,
        ),
      );
      notifyListeners();
    });
  }
}
